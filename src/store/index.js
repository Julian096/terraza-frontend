import Vue from 'vue'
import Vuex from 'vuex'
import router from "../router";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    numberPage: 1,
    rol: 1,
    normalUser: {
      numberHouse: "",
      name: "",
      rol: 0,
    },
    specialUser: {
      username: "",
      rol: 0
    }
    
  },
  mutations: {
    saveUserData(state, data) {
      state.rol = data.rol;
      if(data.rol == 0) {
        state.specialUser.username = data.username;
        state.specialUser.rol = data.rol;
      } else {
        state.normalUser.name = data.name;
        state.normalUser.numberHouse = data.numberHouse;
        state.normalUser.rol = data.rol;
      }
    },
    deleteUserData(state) {
      if(state.rol == 0) {
        state.specialUser.username = "";
        state.specialUser.rol = 0;
      } else {
        state.normalUser.name = "";
        state.normalUser.numberHouse = "";
        state.normalUser.rol = 0;
      }
      sessionStorage.removeItem("token");
      state.numberPage = 1;
      router.push({ name: "Login" });
    },
    changeNumberPage(state, number) {
      state.numberPage = number;
    }
  },
  actions: {
  },
  modules: {
  }
})
