import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/normal',
    name: 'Normal',
    component: () => import('../views/normalUsers/Normal.vue')
  },
  {
    path: '/special',
    name: 'Special',
    component: () => import('../views/specialUsers/Special.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
